
import PushNotification from 'react-native-push-notification';
import Toast from 'react-native-toast-message';
import AsyncStorage from '@react-native-community/async-storage';
//import {useAuth} from './auth';
//const {state}=useAuth();


class NotificationHandler {

  
  
  
  onNotification(notification) {
    console.log('NotificationHandler-notify:', notification);
     this.phpNotification=notification;  
    
   
    
    this.generatePopUp();
       
    if (typeof this._onNotification === 'function') {
      this._onNotification(notification);
    }
  }

  onRegister(token) {
   
    console.log('NotificationHandler-token:', token);
   
     this.storeToken(token);
   

    if (typeof this._onRegister === 'function') {
      this._onRegister(token);
    }
  }

  onAction(notification) {
    console.log ('Notification action received:');
    console.log(notification.action);
    console.log(notification);

    if(notification.action === 'Yes') {
      PushNotification.invokeApp(notification);
    }
  }

  // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
  onRegistrationError(err) {
    console.log(err);
  }
  
  attachRegister(handler) {
    this._onRegister = handler;
  }

  attachNotification(handler) {
   
    this._onNotification = handler;
    return this._onNotification;
  }

  
  generatePopUp(){
    
    constructNotification(this.phpNotification);
    
    return (this.phpNotification);
  }

 storeToken(token)
 {
   storemyToken(token);
 }

}

export const storemyToken=async(token)=>{
  var ctoken=JSON.stringify(token);
  AsyncStorage.removeItem('token');
  AsyncStorage.setItem('token',ctoken);
}

export const constructNotification  = async(notif) =>{

  //alert(JSON.stringify(notif));
  
  var msg=JSON.parse(JSON.stringify(notif));
  console.log("",msg);
  var msgData=JSON.parse(JSON.stringify(msg.data.data));
  console.log(JSON.parse(JSON.stringify(msgData)));
  var str = msgData.replace(/\\/g, '');
  let fmsg=JSON.parse(str)
  console.log(fmsg.message);
   let utype=await AsyncStorage.getItem('usertype');
    
   if(utype!='User')
   {
   Toast.show({
      text1: 'Jaswal & Co - User Activity',
      text2:  fmsg.message
    });
   }
  

}
const handler = new NotificationHandler();

PushNotification.configure({
  // (optional) Called when Token is generated (iOS and Android)
  onRegister: handler.onRegister.bind(handler),

  // (required) Called when a remote or local notification is opened or received
  onNotification: handler.onNotification.bind(handler),

  // (optional) Called when Action is pressed (Android)
  onAction: handler.onAction.bind(handler),

  // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
  onRegistrationError: handler.onRegistrationError.bind(handler),

  // IOS ONLY (optional): default: all - Permissions to register.
  permissions: {
    alert: true,
    badge: true,
    sound: true,
  },

  // Should the initial notification be popped automatically
  // default: true
  popInitialNotification: true,

  /**
   * (optional) default: true
   * - Specified if permissions (ios) and token (android and ios) will requested or not,
   * - if not, you must call PushNotificationsHandler.requestPermissions() later
   */
  requestPermissions: true,
});

export default handler;