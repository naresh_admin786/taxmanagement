import React, {Component} from "react";
//import PushNotification from "react-native-push-notification";
import firebase from 'react-native-firebase';
import {View,Text} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

// var PushNotification = require("react-native-push-notification");
import PushNotification from "react-native-push-notification";
import PushNotificationIOS from "@react-native-community/push-notification-ios"
export default class PushController extends Component{
    componentDidMount(){
   /*   this.checkPermission();
      this.createNotificationListeners(); //add this line
    } 


    componentWillUnmount() {
   this.notificationListener();
      this.notificationOpenedListener();
    }*/
    
      PushNotification.configure({
            // (optional) Called when Token is generated (iOS and Android)
            onRegister: function(token) {
              console.log("TOKEN:", token);
            },
          
            // (required) Called when a remote or local notification is opened or received
            onNotification: function(notification) {
              console.log("NOTIFICATION:", notification);
             // alert(JSON.stringify(notification));
          
              // process the notification here
          
              // required on iOS only 
              notification.finish(PushNotificationIOS.FetchResult.NoData);
            },
             // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
  onAction: function (notification) {
    console.log("ACTION:", notification.action);
    console.log("NOTIFICATION:", notification);

    // process the action
  },

   // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
   onRegistrationError: function(err) {
    console.error(err.message, err);
  },

            // Android only
            senderID: "901072398067",
            // iOS only
            permissions: {
              alert: true,
              badge: true,
              sound: true
            },
            popInitialNotification: true,
            requestPermissions: true
          
      })    
      PushNotification.getChannels(function (channel_ids) {
        console.log(channel_ids); // ['channel_id_1']
      });
/*
     //1
async checkPermission() {
  const enabled = await firebase.messaging().hasPermission();
  if (enabled) {
      this.getToken();
  } else {
      this.requestPermission();
  }
}

  //3
async getToken() {
  
  let fcmToken = await AsyncStorage.getItem('fcmToken');
  
  if (!fcmToken) {
    
      fcmToken = await firebase.messaging().getToken();
      console.log("fcmToken",fcmToken);
      if (fcmToken) {
          // user has a device token
          await AsyncStorage.setItem('fcmToken', fcmToken);
      }
  }
 
}


  //2
async requestPermission() {
  try {
      await firebase.messaging().requestPermission();
      // User has authorised
      this.getToken();
  } catch (error) {
      // User has rejected permissions
      console.log('permission rejected');
  }

}


//Remove listeners allocated in createNotificationListeners()

async createNotificationListeners() {
  /*
  * Triggered when a particular notification has been received in foreground
  * 
  this.notificationListener = firebase.notifications().onNotification((notification) => {
    alert(JSON.stringify(notification));
      const { title, body } = notification;
      this.showAlert(title, body);
  });

  /*
  * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
  * 
  this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
      const { title, body } = notificationOpen.notification;
      this.showAlert(title, body);
  });

  /*
  * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
  * 
  const notificationOpen = await firebase.notifications().getInitialNotification();
  if (notificationOpen) {
      const { title, body } = notificationOpen.notification;
      this.showAlert(title, body);
  }
  /*
  * Triggered for data only payload in foreground
  * 
  this.messageListener = firebase.messaging().onMessage((message) => {
    //process data message
    Alert.alert('A new FCM message arrived!', JSON.stringify(message));
    console.log(JSON.stringify(message));
  });
}




showAlert(title, body) {
  alert(
    title, body,
    [
        { text: 'OK', onPress: () => console.log('OK Pressed') },
    ],
    { cancelable: false },
  );

   */  

  }
    render(){
      return null;
    }
}