import React,{useEffect,useRef,useState} from 'react';
import { 
    View, 
    Text, 
    TouchableOpacity, 
    Dimensions,
    StyleSheet,
    StatusBar,
    Image,
    ImageBackground,
    AppState
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { useTheme } from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';
import {useAuth} from '../providers/auth';
import * as api from "../api/auth";


const exampleImage2 = require('../assets/footerback.jpg');
//const appState = useRef(AppState.currentState);
//const [appStateVisible, setAppStateVisible] = useState(appState.current);
const SplashScreen = ({navigation}) => {
  
    const {state,isLoggedIn, handleLogin } = useAuth();
 
      useEffect(() => {
       
        AppState.addEventListener("change", _handleAppStateChange);
        _recoverData();
        return () => {
          AppState.removeEventListener("change", _handleAppStateChange);
        };
      }, []);




const _handleAppStateChange = (nextAppState) => {


   /* if (
        appState.current.match(/inactive|background/) &&
        nextAppState === "active"
      ) {
        console.log("App has come to the foreground!");
        _recoverData();  
    }
  
      appState.current = nextAppState;
      setAppStateVisible(appState.current);
      console.log("AppState", appState.current);
        */


    if (nextAppState === 'background' || nextAppState === 'inactive') {
        _storeData('dataToSave');
    }
  }
       
      const _storeData=(params)=>{

      }
       const _recoverData=async()=>{


         let stored_uname=await AsyncStorage.getItem('uname');
         let pass=await AsyncStorage.getItem('passw');

         
         if(stored_uname!=null)
         {
             
            //alert(stored_uname+'  '+pass);
            loginHandle(stored_uname,pass);
            //return;
         }else{
            setTimeout( () => {
            
                setTimePassed();
             },2000);
         }

         checkInternet();

       } 
        
      const checkInternet=async()=>{
        const isConnected = await NetworkUtils.isNetworkAvailable();
        if(!isConnected)
        {
            console.log('No Internet Connection Available');
            alert('No Internet Connection Available');
            
            return;
        }
      }
    const { colors } = useTheme();
    
        
      
      
      const setTimePassed=()=> {
         //this.setState({timePassed: true});
         
         navigation.navigate('SignInScreen');
         
      }
      
    
      const loginHandle =async(userName, password) => {
       
          try {
           let response = await api.login(userName,password);
           if(response.count==0)
           {
               alert('user does not exists!');
               
               return;
           }
           if(response.body=='' || response.body==null || response==null)
           {
               alert('Something went wrong');
           }
         
           await handleLogin(response);
           //alert(JSON.stringify(response.body[0].emailid));
          
           

           //check if username is null
           let username = (response.body[0].username !== null);
           if (username) 
           {
             
             //  await AsyncStorage.setItem('uname',response.body[0].username);
              // await AsyncStorage.setItem('passw',password);
              navigation.reset({
                index: 0,
                routes: [{ name: 'Home' }],
              });  
           navigation.navigate('Home',{uname:response.body[0].username});
           
          
           }
           
       } catch (error) {
           alert(error.message);
                  }


   }
     

    return (
      <View style={styles.container}>
          <StatusBar backgroundColor='#F6F9F3' barStyle="light-content"/>
        <View style={styles.header}>
            <Animatable.Image 
                animation="bounceIn"
                duraton="1500"
            source={require('../assets/logo.png')}
            style={styles.logo}
            resizeMode="stretch"
            />
        </View>
       
        <Animatable.View 
            style={[styles.footer, {
                backgroundColor: '#F6F9F3'
            }]}
            animation="fadeInUpBig"
        > 
        
       
            <Text style={[styles.title, {
                color: colors.text
            }]}>Manage your Tax!</Text>
            <Text style={styles.text}>Sign in with account</Text>
            <View style={styles.button}>
           
            <TouchableOpacity onPress={()=>_recoverData()}>
                <LinearGradient
                    colors={['#FFA07A', '#FFF080']}
                    style={styles.signIn}
                >
                    <Text style={styles.textSign}>Get Started</Text>
                    <MaterialIcons 
                        name="navigate-next"
                        color="#fff"
                        size={20}
                    />
                </LinearGradient>
            </TouchableOpacity>
           
            </View>
           
        </Animatable.View>
        <ImageBackground source={exampleImage2} style={styles.image}></ImageBackground>
      </View>
      
    );
};

export default SplashScreen;

const {height} = Dimensions.get("screen");
const height_logo = height * 0.28;

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    backgroundColor: '#F6F9F3'
  },
  header: {
      flex: 2,
      justifyContent: 'center',
      alignItems: 'center'
  },
  footer: {
      flex: 1,
      backgroundColor: '#F6F9F3',
      borderTopLeftRadius: 30,
      borderTopRightRadius: 30,
      paddingVertical: 50,
      paddingHorizontal: 30,
     
  },
  logo: {
      width: height_logo,
      height: height_logo
  },
  title: {
      color: '#05375a',
      fontSize: 30,
      fontWeight: 'bold'
  },
  text: {
      color: 'grey',
      marginTop:5
  },
  button: {
      alignItems: 'flex-end',
      marginTop: 30
  },
  signIn: {
      width: 150,
      height: 40,
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 50,
      flexDirection: 'row'
  },
  textSign: {
      color: 'white',
      fontWeight: 'bold'
  } ,image: {
    flex:0.5,
    resizeMode: "cover",
    justifyContent: "flex-start",
    width:'100%'
  },
});